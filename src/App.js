import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter, Switch, Route, NavLink, Redirect} from 'react-router-dom'
import Login from "./components/Login";
import Profile from "./components/Profile"
import Translate from "./components/Translate";
import Register from './components/Register';

function App() {
  return (
      <BrowserRouter>
        <Switch>
          
          <Route path="/" exact component={Login} /> 
          {/* <Route path="/login" component={Login}/> */}
          <Route path="/translate" component={Translate}/>
          <Route path="/register" component={Register}/>
          {/* <Route path="/*" component={NotFound}/> */}

        </Switch>
      </BrowserRouter>
    /*   <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */
    
  );
}

export default App;
