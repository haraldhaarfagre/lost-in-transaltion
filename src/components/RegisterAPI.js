export const RegisterAPI = {
    register({ username, password }) {
        console.log('inside the register api')
        return fetch('https://noroff-react-txt-forum-api.herokuapp.com/users/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ username, password })
        }).then(async response => {
            if(!response.ok){
                const {error = 'Some error occured.'} = await response.json()
                throw new Error(error)
            }
            return response.json()
        })

    }
}