import React, { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { Redirect, Link } from "react-router-dom"
import { registerAttemptAction } from "../store/actions/registerActions"

const Register = () => {

    const dispatch = useDispatch();
    const { loggedIn } = useSelector(state => state.sessionReducer);

    const [user, setUser ] = useState({
        username: '',
        password: '',
        repeat_password: ''
    });

    const onRegisterSubmit = event => {
        event.preventDefault()
        console.log('Register.onRegisterSubmit()', user)
        dispatch(registerAttemptAction(user))
    };

    const onInputChange = event => {
        
        setUser({
            ...user,
            [event.target.id]: event.target.value
        });
    };
    return (
        <div className="ml-3">
            {loggedIn && <Redirect to="/translate" />}
            <form className="mb-3" onSubmit={onRegisterSubmit}>
                <h1>Register for lost in translation</h1>
                <p>Complete the form to create your account</p>

                <div className="mb-3">
                    <label htmlFor="username" className="form-label">Choose a username</label>
                    <input id="username" type="text" onChange={onInputChange}
                        placeholder="johndoe" className="form-control"></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="password" className="form-label">Password</label>
                    <input id="password" type="text" onChange={onInputChange}
                        placeholder="******" className="form-control"></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="repeat_password" className="form-label">Repeat Password</label>
                    <input id="repeat_password" type="text" onChange={onInputChange}
                        placeholder="******" className="form-control"></input>
                </div>

                <button className='btn btn-success btn-lg'>Register</button>
            </form>
            <p className='mb-3'>
               <Link to='/'>Already registered? Login here</Link>
            </p>
        </div>
    )
}

export default Register