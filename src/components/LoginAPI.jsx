export const LoginAPI = {
    login(credentials) {
        /* need to find the url */
        return fetch('https://noroff-react-txt-forum-api.herokuapp.com/users/login', {
        //return fetch('http://localhost:3000/profile', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        })
        .then(async (response) => {
            if(!response.ok){
                const {error = 'An unknown error occured' } = await response.json()
                throw new Error(error)
            }
            return response.json()
        })
    }
}