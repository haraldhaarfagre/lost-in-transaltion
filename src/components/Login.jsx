import { Redirect, Link } from 'react-router-dom'
import React, { useState } from "react";
import { useDispatch, useSelector } from 'react-redux';
import { loginAttemptAction } from '../store/actions/loginActions';

const Login = () => {

    const dispatch = useDispatch();
    const { loginError, loginAttempting } = useSelector(state => state.loginReducer)
    const { loggedIn } = useSelector(state => state.sessionReducer)

    const [credentials, setCredentials] = useState({
        username: '',
        password: ''
    });

    const onInputChange = event => {
        setCredentials({
            ...credentials,
            [event.target.id]: event.target.value
        });
    };

    const onFormSubmit = event => {
        event.preventDefault() //stop reload
        dispatch(loginAttemptAction(credentials))
    }

    return (
        <>
            { loggedIn && <Redirect to="/translate"/> }
            { !loggedIn &&
                <main className="container" className="mt-3 mb-3 ml-3 mr-3">
                <h1>Login to translate</h1>
                <form className="mt-3 mb-3" onSubmit={onFormSubmit}>
                    <div className="mb-3">
                        <label htmlFor="username" className="form-label">Username</label>
                        <input id="username" type="text" onChange={onInputChange}
                            placeholder="Enter your unique username" className="form-control"></input>
                    </div>

                    <div className="mb-3">
                        <label htmlFor="password" className="form-label">Password</label>
                        <input id="password" type="password" onChange={onInputChange}
                            placeholder="Enter your unique username" className="form-control"></input>
                    </div>

                    <button type="submit" className="btn btn-primary btn-lg">Login</button>

                </form>

                {loginAttempting &&
                    <p>Trying to log in...</p>
                }
                {loginError &&
                    <div className="alert alert-danger" role="alert">
                        <h4>Unsuccessful</h4>
                        <p className="mb-0">{loginError}</p>
                    </div>
                }
                <p className='mb-3'>
                    <Link to='/register'>Not registered? Register here</Link>
                </p>
            </main>
            }
        </>

    );
};

export default Login