import { useState } from "react"

const Translate = () => {

    const [translation, setTranslation] = useState({
        textToTranslate: '',
        pictures: []
    })

    const onInputChange = event => {
        setTranslation({
            ...translation,
            [event.target.id]: event.target.value,
        });
        //fillPictures();
    }
    // const fillPictures = () => {

    //     for (var i=0; i<translation.textToTranslate; i++){
    //         console.log(translation.textToTranslate);
    //         translation.pictures.push(findImage(translation.textToTranslate[i]))
    //     }
    // }
   
    function RenderImage() {
        const text = translation.textToTranslate
        var rows = [];
        console.log('inside render image')

        for (var i = 0; i < text.length; i++) {
            rows.push(<ul key={i}><img src={findImage(text[i])} /></ul>)
        }
        return rows;
    }

    const findImage = letter => {
        return letter + '.png'
    }

    return (
        <main className="mt-3 mb-3 ml-3 mr-3">
            <h1>Translate</h1>
            {/* add username if time */}
            {/* <form className="mt-3 mb-3" onSubmit={onFormSubmit}> */}
                <div className="mt-3 mb-3 ml-3 mr-3">
                    <label>Input text to translate</label><br></br>
                    <input className="form-control" type="text" id="textToTranslate" onChange={onInputChange}></input>
                </div>
                {/* <button type="submit" className="btn btn-primary btn-lg">Translate</button> */}
            {/* </form> */}
            <div>
                <RenderImage />
            </div>
        </main>
    )
}

export default Translate