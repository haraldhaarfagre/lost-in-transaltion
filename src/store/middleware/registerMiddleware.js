import { RegisterAPI } from "../../components/RegisterAPI"
import { ACTION_REGISTER_ATTEMPTING, ACTION_REGISTER_SUCCESS, registerErrorAction, registerSuccessfullAction } from "../actions/registerActions"


export const registerMiddleware = ({ dispatch }) => next => action => {

    next(action)
    console.log('inside the register middleware')
    if (action.type === ACTION_REGISTER_ATTEMPTING){
        //console.log(action.payload)
        RegisterAPI.register(action.payload)
        .then(profile => {
            dispatch(registerSuccessfullAction(profile))
        })
        .catch(error => {
            dispatch(registerErrorAction(error.message))
        })

    }

    if (action.type === ACTION_REGISTER_SUCCESS)
    {
        //YAY i have registered
    }
}