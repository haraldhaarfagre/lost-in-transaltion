import { applyMiddleware } from "redux";
import { loginMiddleware } from "./loginMiddleware";
import { registerMiddleware } from "./registerMiddleware";
import { sessionMiddleware } from "./sessionMiddleware";

export default applyMiddleware(
    loginMiddleware,
    sessionMiddleware,
    registerMiddleware
)