import { LoginAPI } from "../../components/LoginAPI"
import { ACTION_LOGIN_ATTEMPTING, ACTION_LOGIN_SUCCESS, loginErrorAction, loginSuccessfullAction } from "../actions/loginActions"
import { sessionSetAction } from "../actions/sessionActions"

export const loginMiddleware = ({ dispatch }) => next => action => {
    
    next(action)

    if (action.type === ACTION_LOGIN_ATTEMPTING){
        LoginAPI.login(action.payload)
        .then(profile => {
            dispatch(loginSuccessfullAction(profile))
        })
        .catch(error => {
            dispatch(loginErrorAction(error.message))
        })
    }
    if (action.type === ACTION_LOGIN_SUCCESS) {
        // WOW. nice work now i am logged in  -> Do something about it
        dispatch(sessionSetAction(action.payload))

    }
}